require('dotenv').config();
const express = require('express');
const app = express();
const compression = require('compression');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const crypto = require('crypto');
const moment = require('moment');
const _ = require('lodash');
const path = require('path');
const Agenda = require('../worker/agenda');
const EventsApi = require('../lib/events/api');
const Apps = require('../lib/apps/api');

class App {
    constructor(app) {
        this.app=app;
        this.setupExpress();
        this.setupApi();
        this.setupAgendaTriggers();
        this.setupDefaultRoutes();
        this.setupErrorHandler();
    }

    static init() {
        return new App(express()).app;
    }

    /**
     * setup Express middlewares
     */
    setupExpress() {
        this.app.use(compression());
        this.app.use(helmet({
            frameguard: {
                action: 'deny'
            }
        }));
        this.app.use(helmet.hidePoweredBy({
            setTo: 'PHP 5.3.0'
        }));
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
        this.app.use(cookieParser(process.env.COOKIE_SECRET));
        // register docs to express server
        this.app.use('/docs', express.static(path.resolve(__dirname, '../docs/')));
    }
    /**
     * Setup server default routes i.e. /, /ping
     */
    setupDefaultRoutes() {
        // ROOT
        this.app.get('/', (req, res) => {
            return res.json({
                name: process.env.API_NAME,
                version: process.env.API_VERSION,
                message: 'Welcome to the Events API, visit the documentation for information on usage and the API',
                routes: {
                    message: 'The routes available are',
                    docs: '/docs/',
                    api: {
                        main: '/api/v1/',
                        events: '/api/v1/events/',
                        testingAccess: '/api/test/'
                    }
                },
                timestamp: moment().toString()
            });
        });
    }
    /**
     * Setup Error handler for the REST API
     */
    setupErrorHandler() {
        // 404 handler
        this.app.use((req, res, next) => {
            const err = new Error();
            err.message = 'Requested Resource is not found';
            err.name = 'NotFoundError';
            err.status = 404;
            err.level = 'warning';
            return next(err);
        });
        // error handler middleware
        this.app.use((err, req, res, next) => {
            console.log(err); // for debugging purposes
            return res.status(_.has(err, 'status') ? err.status : 500).json(err || 'Something Went Wrong!');
        });
    }
    /**
     * setup all the apis from /lib folder
     */
    setupApi() {
        const v1 = express.Router();
        v1.use('/v1/events', EventsApi);
        v1.use('/v1/apps', Apps.api);
        
        this.app.use('/api', v1);
    }
    setupAgendaTriggers() {
        const trigger = express.Router();
        // @route: trigger Events Scrapper
        trigger.get('/events-scraper', (req, res) => {
            Agenda.now('start scrap processor');
            res.sendStatus(201);
        });

        this.app.use('/trigger', trigger);
    }
}

module.exports = App.init();