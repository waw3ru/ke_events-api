require('dotenv').config();
const SendGrid = require('@sendgrid/mail');

function sendMail(to, from, subject, templateId, data) {
    SendGrid.setApiKey(process.env.SENDGRID_KEY);
    SendGrid.setSubstitutionWrappers('{{', '}}');

    const msg = {to, from, subject, templateId};
    msg.category = ['developer', 'credentials', 'security'];
    msg.substitutions = data;
    // console.log(msg);
    return SendGrid.send(msg);
}

module.exports = sendMail;