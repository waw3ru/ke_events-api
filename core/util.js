const _ = require('lodash');
const moment = require('moment');


/**
 * a utility function for constructing a date range 
 * @param {string} start a date string
 * @param {string} end a date string
 * @returns {Object}
 */
module.exports.buildDateRange = (start, end) => {
    const sdt = moment(`${start} 00:00`, 'MM-DD-YYYY HH:mm');

    let edt;
    // create an end date which is addition of the start date
    if (!end || moment(end, 'MM-DD-YYYY').isSame(sdt, "day")) edt = moment(sdt).add(1, 'day');
    // create an end date where time is almost midnight that day
    else edt = moment(`${end} 23:59`, 'MM-DD-YYYY HH:mm');

    if (!moment(sdt).isValid() || !moment(edt).isValid()) {
        const err = new Error();
        err.name = 'DateRangeError';
        err.message = 'The provided dates are not valid, please follow the format: MM-DD-YYYY. E.g: 04-14-2018';
        err.status = 500;
        err.level = 'error';

        return {
            error: err,
            hasError: true
        };
    }

    return {
        startDate: sdt.toDate(),
        endDate: edt.toDate(),
    };
};

/**
 * used to create pages for the records to limit amount of data returned
 * @param {number} num total number of records in the database
 */
module.exports.paginateRecords = (num, page, items) => {
    const isMultiple = (num % 10 !== 0);
    const pages = isMultiple ? Math.ceil(num/items) : Math.floor(num/items);

    if (num < (page*items)) {
        const err = new Error();
        err.name = 'PaginationError';
        err.message = 'The page requested surpasses the number of items per page. No data will be returned';
        err.status = 400;
        err.level = 'warning';
        return Promise.reject(err);
    }

    // deal with the last page of the pagination
    if ((page * items) < num && ((page + 1) * items) >= num) {
        return Promise.resolve({
            skip: page * items,
            limit: items,
            end: true,
            totalPages: pages
        });
    }

    return Promise.resolve({
        skip: page * items,
        limit: items,
        end: false,
        totalPages: pages
    });

}
