require('dotenv').config();

const mongoose = require('mongoose');
const bluebird = require('bluebird');

bluebird.config({
    warnings: false,
    longStackTraces: false
});

class Database {

    static connect(pool=10) {
        mongoose.Promise = bluebird;
        const conn = mongoose.connection;
        
        /* @event DatabaseOpen */
        conn.once('open', () => {
            console.log('Connected to db');
        });
        /* @event DatabaseClose */
        conn.on('disconnected', () => {
            console.log('disconnected from db');
        });
        /* @event DatabaseConnectionError */
        conn.on('error', e => {
            console.log(`Mongoose connection error ${e}`)
        });

        return mongoose.connect(process.env.MONGO_URI,
            {
                poolSize: pool,
                autoReconnect: true,
                reconnectTries: Number.MAX_VALUE,
                reconnectInterval: 4500,
                promiseLibrary: bluebird,
                keepAlive: 1,
                connectTimeoutMS: 30000
            }
        )
        .catch(e => {
            mongoose.disconnect();
            return Promise.reject(e);
        });
    }

    static gracefullShutdown(message, cb) {
        mongoose.connection.close(() => {
            console.log(`Mongoose disconnected through ${message}`);
            cb();
        });
    };
}

module.exports = Database;