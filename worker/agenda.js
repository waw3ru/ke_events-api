
require('dotenv').config();
const os = require('os');
const Agenda = require('agenda');

// agenda instance
const agenda = new Agenda({
    db: {
        address: process.env.MONGO_URI,
        collection: 'EVENTS254_CRON_JOBS'
    },
    name: `[events254--${os.platform}]`
});

const db = require('../core/database');
const EventsCrawler = require('./events-crawler');
const BusinessDirectoryCrawler = require('./business-directory-crawler');
const SendMail = require('../core/email');

//# JOBS
agenda.define('Test Job', (job, done) => {
    console.log('data sent by client: ', job.attrs.data);
    done();
});
agenda.define('start scrap processor', (job, done) => {
    db.connect();
    EventsCrawler(() => done()); // start event crawler
});
agenda.define('download business directory', (job, done) => {
    db.connect();
    BusinessDirectoryCrawler((err, info) => {
        if (err) done(err);
        else done();
    });
});
agenda.define('send e-mail', (job, done) => {
    const {to, from, subject, templateId, data} = job.attrs.data;
    SendMail(to, from, subject, templateId, data).then(res => done()).catch(err => done(err));
});
//# END JOBS

module.exports = agenda;
