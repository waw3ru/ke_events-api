/**
 * created by waweru
 */

const agenda = require('./agenda');

 /**
  * start agenda processor and set default schedules
  * @event onReady
  */
agenda.on('ready', () => {

    agenda.every('6 days', 'start scrap processor');
    agenda.every('28 days', 'download business directory');
    console.log('Agenda is ready to start...'); // #log

    // start agenda
    agenda.start();
});
/**
 * logs out errors that occur with agenda processor
 * @event onError
 */
agenda.on('error', err => {
    console.log(err); // #log
});
/**
 * logs out when a specific job finishes execution
 * @event onComplete
 */
agenda.on('complete', job => {
    console.log('Finished: ' + job.attrs.name); // #log
});
/**
 * logs out when a job starts
 * @event onStart
 */
agenda.on('start', job => {
    console.log('Started: ' + job.attrs.name); // #log
});
