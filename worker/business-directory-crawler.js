const mongoose = require('mongoose');
const BusinessDirectoryCrawler = require('../lib/business-directory/crawler');
const _ = require('lodash');

const Crawler = new BusinessDirectoryCrawler(); // initialize Crawler class
const Model = require('../lib/business-directory/model');

/**
 * Start the business directories scarper
 * @param {Array} categories an Array of business directory categories
 * @param {number} idx array index from the categories Array 
 * @param {Function} done 
 */
function startDirectoryScraper(categories, idx, done) {
    process.setMaxListeners(Infinity);

    if (_.isNil(categories[idx])) {
        return done(`stopped at index: ${idx}`);
    }

    scraper2(categories[idx])
        .then(data => {
            console.log(`Crawler done crawling`);

            console.log(`Record Count: ${data.length}`);
            // console.log(`Record sample: ${JSON.stringify(data[0])}`);
            return data;
        })
        // .then(data => Promise.all(data.map((v) => console.log(v))))
        .then(data => Promise.all(data.map(async (v) => await require('../lib/business-directory/model').add(v))))
        .then(() => Crawler.pause('Crawler paused processing... \n'))

        .then(startDirectoryScraper.bind(null, categories, idx + 1, done))
        .catch(e => {
            console.log('Crawler stopped running...\n');
            console.log(e);
            done(`stopped at index: ${idx}`);
        });
}

/**
 * business directory scraper1 for getting category,title and link of a business directory
 */
const scraper1 = () => new Promise((resolve, reject) => {
    Crawler.scrapCategories([], (e, data) => {
        if (!_.isNil(e)) {
            reject(e);
        }
        resolve(data);
    });
});

/**
 * business directory scraper2 for getting directory profile data like contacts and map link
 */
const scraper2 = category => new Promise((resolve, reject) => {
    Crawler.scrapDirectories(category, [], (e, data) => {
        if (!_.isNil(e)) {
            reject(e);
        }
        resolve(data);
    });
});

module.exports = (done) => {
    scraper1()
        .then(data => {
            startDirectoryScraper(data, 0, info => {
                done(null, info);
            });
        })
        .catch(err => {
            done(err);
        });
};