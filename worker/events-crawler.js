const mongoose = require('mongoose');
const EventsCrawler = require('../lib/events/crawler');

const Crawler = new EventsCrawler(); // initialize Crawler class

/**
 * runs the crawling process till it encounters an error and stops
 * @param {Function} done callback function
 * @param {number} pageNo page number
 */
function start(done, pageNo=1) {
    process.setMaxListeners(Infinity);

    Crawler.run(pageNo)
        .then(data => { console.log(`Crawler done crawling Page No: ${pageNo}`); return data; })
        .then(data => Promise.all(data.map(async (v) => await require('../lib/events/model').create(v))))
        .then(data => { console.log('Crawler done saving data.\n'); return null; })
        .then(() => Crawler.pause('Crawler paused processing... \n', 1000))
        .then(start.bind(null, done, pageNo+1))
        .catch(e => {
            console.log('Crawler stopped running...\n');
            console.log(e);
            mongoose.disconnect();
            return done();
        });
}

module.exports = start;