require('dotenv').config();

const http = require('http');
const debug = require('debug')('Events254:API');
const db = require('./core/database');
const app = require('./core/app');

// connect to the database
db.connect(40);

// setup server port
const port = normalizePort(process.env.PORT || 8080);
app.set('port', port);
app.set('env', process.env.ENV);

// bootstrap server from http module
const server = http.createServer(app);

// start server
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * provide the appropriate data type of the port which is number
 * @param {*} val port number
 */
function normalizePort(val) {
    const port = (typeof val === 'string') ? parseInt(val, 10) : val;

    if (isNaN(port)) {
        return val;
    } else if (port >= 0) {
        return port;
    } else {
        return false;
    }
}
/**
 * when the server errors out when it tries starting
 * @param {Error} error Http Server error object
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}
/**
 * log the port number after the server successfully starts
 */
function onListening() {
    let addr = server.address();
    let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
    console.log(`Listening on ${bind}`);
}

/**
 * Handle Restarts (Close Database Connection) for nodemon restarts
 */
process.once('SIGUSR2',
    () =>  db.gracefullShutdown('nodemon restart',  () => process.kill(process.pid, 'SIGUSR2')));
/**
 * For app termination
 */
process.on('SIGINT', () => db.gracefullShutdown('App termination', () => process.exit(0)));
/**
 * For Heroku app termination
 */
process.on('SIGTERM', () => db.gracefullShutdown('Heroku App termination', () => process.exit(0)));

debug('Events254 Server');
