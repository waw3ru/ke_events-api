
const gulp = require('gulp')
const del = require('del');
const path = require('path');

// deploy docs to api server
gulp.task('deploy:docs', () => {
    return gulp.src(path.resolve(__dirname, '../docs/build') + '/**/*')
        .pipe(gulp.dest(path.resolve(__dirname, 'docs/')));
});
// clean docs
gulp.task('clean:docs', cb => {
    return del(['./docs/**/*'], cb);
});