const _ = require('lodash');
const schema = require('./schema');
const moment = require('moment');
const Helpers = require('../../core/util');

class BusinessDirectoryModel {

    /**
     * initializes the class
     * @static
     */
    static init() {
        return new BusinessDirectoryModel(schema);
    }

    /**
     * returns the database model (mongoose Schema) being used by the BusinessDirectoryModel
     * @public 
     */
    get dbModel() {
        return this._model;
    }

    /**
     * @constructor
     * @param {*} schema mongoose model
     */
    constructor(schema) {
        this._model = schema;
    }

    /**
     * adds a new business directory
     * @param {Object} data 
     */
    add(data) {
        const newDirectory = new this._model(data);
        return newDirectory.save()
            .catch(e => {
                if (e && e.code === 11000) return Promise.resolve(null);

                const err = new Error();
                err.name = e.name || 'CreationError';
                err.message = e.message || 'Something went wrong while saving business directory';
                err.status = 500;
                err.level = 'error';
                return Promise.reject(err);
            });
    }
}

module.exports = BusinessDirectoryModel.init();