/**
 * created by waweru 
 */

require('dotenv').config();

const Osmosis = require('osmosis');
const Fs = require('fs');
const moment = require('moment');
const _ = require('lodash');

/**
 * Business directory crawler
 * @class BusinessDirectoryCrawler
 * @see https://www.kenyabuzz.com/directory/
 */
class BusinessDirectoryCrawler {

    /**
     * contains the business directory categories scraper
     * @param {Function} done callback function
     * @param {Array} json data container
     */
    scrapCategories(json, done) {
        Osmosis.get('https://www.kenyabuzz.com/directory/')
        .find('div.coloum-wrapper .col-sm-4 .category--light')
        .set({
            parentCategory: '.category__title'
        })
        .find('ul.list.line--marker li')
        .set({
            link: 'a@href',
            category: 'a'
        })
        .data(dataCollected => json.push(dataCollected))
        .error(e => done(e, undefined))
        .done(() => {
            done(null, json);
        });

    }

    /**
     * contains business directories scrapers
     * @param {string} category business directory category
     * @param {Array} json data container
     * @param {Function} done callback function
     */
    scrapDirectories(category, json, done) {
        Osmosis.get(category.link)
        .find('.coloum-wrapper .col-sm-12 .category--light ul.list.line--marker li')
        .follow('a@href')
        .find('.container .col-sm-8.col-md-9')
        .set({
            title: '.page-heading',
            map: '.post .col-sm-5.col-md-5 .map iframe@src',
            contactInfo: {
                info_1: '.col-sm-7.col-md-7 .listing[1]',
                info_2: '.col-sm-7.col-md-7 .listing[2]',
                info_3: '.col-sm-7.col-md-7 .listing[3]',
                info_4: '.col-sm-7.col-md-7 .listing[4]',
                info_5: '.col-sm-7.col-md-7 .listing[5]',
            },
        })
        .data(dataCollected => {
            if (!_.isNil(dataCollected.contactInfo)) {
                let newInfo = {};
                Object.keys(dataCollected.contactInfo)
                    .forEach(v => {
                        const sanitize = this.sanitizeContactInfo(dataCollected.contactInfo[v]);
                        newInfo = Object.assign({},
                            newInfo,
                            sanitize,
                        );
                        
                    });

                dataCollected.contactInfo = {...newInfo};
                json.push({ ...category, ...dataCollected });
            } else {
                json.push({ ...category, ...dataCollected });                
            }
        })
        .error(e => done(e, undefined))
        .done(() => {
            done(null, json);
        });
    }

    /**
     * pauses the crawler to avoid stack overflow
     * @param {number} time how much time to pause
     * @param {string} returnTxt return message
     */
    pause(returnTxt, time=500) {
        return new Promise(resolve => {
            _.delay(txt => {
                console.log(returnTxt);
                resolve()
            }, time, returnTxt);
        });
    }

    /**
     * sanitizes contact info from the business directories scrapper
     * @param {string} info contact info 
     */
    sanitizeContactInfo(info) {
        const contactInfoPlaceholders = ['Location', 'Email Address', 'Contact No.', 'Website',];
        const infoArr = info.split(':');
        if (infoArr.length !== 2) {
            return { bio: info.replace(/(\r\n|\n|\r)/gm, '').trim() };
        }
        const idx = _.findIndex(contactInfoPlaceholders, o => o == infoArr[0]);
        if (idx < 0) {
            return null;
        }
        return {
            [_.snakeCase(contactInfoPlaceholders[idx])]: infoArr[1].replace(/(\r\n|\n|\r)/gm, '').trim()
        };
    }

}

module.exports = BusinessDirectoryCrawler;
