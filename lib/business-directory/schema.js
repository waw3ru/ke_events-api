const mongoose = require('mongoose');

const businessDirectorySchema = new mongoose.Schema({
    parentCategory: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    title: {
        type: String,
        unique: true,
        required: true
    },
    link: String,
    map: String
}, {
    timestamps: true,
    strict: false
});

module.exports = mongoose.model('business_directory', businessDirectorySchema, 'Events254_Business_Directory');