const _ = require('lodash');
const schema = require('./schema');
const moment = require('moment');
const Helpers = require('../../core/util');

class EventsModel {

    /**
     * initializes the class
     * @static
     */
    static init() {
        return new EventsModel(schema);
    }

    /**
     * returns the database model (mongoose Schema) being used by the BusinessDirectoryModel
     * @public 
     */
    get dbModel() {
        return this._model;
    }

    /**
     * @constructor
     * @param {*} schema mongoose model
     */
    constructor(schema) {
        this._model=schema;
    }
    /**
     * get events based on Date
     * @param {string} sdt start date
     * @param {string} edt end date
     */
    getByDate(sdt, edt=null) {
        const dateRange = Helpers.buildDateRange(sdt, edt);

        if (_.has(dateRange, 'hasError') && dateRange.hasError) {
            return Promise.reject(dateRange.error);
        }
        return this.get({
            start: { $gte: dateRange.startDate },
            end: { $lte: dateRange.endDate }
        })
        /* .then(data => { console.log(data); return data; }); */
    }
    /**
     * @param {Object} data
     * @param {number} page current page
     * @param {number} items number of items ready to be returned to user
     */
    queryByDate({sdt, edt}=data, page, items) {
        const dateRange = Helpers.buildDateRange(sdt, edt);

        if (_.has(dateRange, 'hasError') && dateRange.hasError) {
            return Promise.reject(dateRange.error);
        }
        return this.query({
            start: { $gte: dateRange.startDate },
            end: { $lte: dateRange.endDate }
        }, page, items);
    }
    /**
     * get a list of all application which have ever hosted an event
     */
    getLocation() {
        return this.get()
        .then(docs => {
            if (_.isNil(docs) || _.isEmpty(docs)) {
                const err = new Error()
                err.name = 'EmptyRecord';
                err.message = 'No record was found in the database';
                err.status = 404;
                err.level = 'warning';
                return Promise.reject(err);
            }
            return docs;
        })
        .then(docs => Promise.all(_.map(docs, v => v.location)))
        .then(locs => Promise.all(_.uniq(locs)));
    }
    /**
     * add a new event to the database
     * @param {Object} data - a event scrapped from the web
     */
    create(data) {
        const newEvent = new this._model(data);
        newEvent.title = data.title.replace(/(\r\n|\n|\r)/gm, '');
        newEvent.about = data.about.replace(/(\r\n|\n|\r)/gm, '');
        newEvent.location = data.location.replace(/(\r\n|\n|\r)/gm, '');

        return newEvent.save()
        .catch(e => {
            if (e && e.code===11000) return Promise.resolve(null);

            const err = new Error();
            err.name = e.name || 'CreationError';
            err.message = e.message || 'Something went wrong while saving events';
            err.status = 500;
            err.level = 'error';
            return Promise.reject(err);
        });
    }
    /**
     * get a list of events based on a query
     * @param {Object} q 
     */
    get(q=null) {
        return this._model.find(q || {}).exec();
    }
    /**
     * get a single event's data using its events id
     * @param {mongoose.Types.ObjectID} id 
     */
    getById(id) {
        return this._model.findOne({ _id: id })
        .exec()
        .then(docs => {
            if (_.isNil(docs) || _.isEmpty(docs)) {
                const err = new Error()
                err.name = 'EmptyRecord';
                err.message = 'No record was found in the database';
                err.status = 404;
                err.level = 'warning';
                return Promise.reject(err);
            }
            return docs;
        });
    }
    /**
     * counts the number of events based on a query
     * @param {Object} q 
     */
    count(q=null) {
        return this._model.count(q || {}).exec();
    }
    /**
     * 
     * @param {Object} q a mongodb query 
     * @param {number} page the current page the user is on
     * @param {number} items the number of items that are to be returned
     */
    query(q=null, page=0, items=10) {
        return this.count(q || {})
        .then(num => Helpers.paginateRecords(num, page, items))
        .then(p => Promise.all([
            this._model.find(q)
                .skip(p.skip)
                .limit(p.limit)
                .sort('-start -end -createdAt')
                .exec(),
            p
        ]))
        .then(([docs, p]) => ({
            data: docs,
            pagination: {
                totalNumOfPages: p.totalPages,
                totalReturnedItems: docs.length,
                isLastPage: p.end
            }
        }));
    }

}

module.exports = EventsModel.init();