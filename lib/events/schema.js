const mongoose = require('mongoose');

const eventsSchema = new mongoose.Schema({
    title: { $type: String, required: true, unique: true },
    image: { $type: String, required: true },
    link: { $type: String, required: true, unique: true },
    about: String,
    start: Date,
    end: Date,
    map: String,
    twitter: String,
    whatsapp: String,
    google_plus: String,
    location: String,
    location_link: String
},
{
    timestamps: true,
    strict: false,
    typeKey: '$type'
});

module.exports = mongoose.model('events', eventsSchema, 'Events_Catalog');
