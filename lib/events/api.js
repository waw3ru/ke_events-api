require('dotenv').config();

const express = require('express');
const EventsModel = require('./model');
const _ = require('lodash');
const crypto = require('crypto');
const { authApp } = require('../apps/util');

class Api {

    /**
     * initializes the class
     */
    static init() {
        const router = express.Router();
        new Api(router);
        return router;
    }

    constructor(app) {
        app.get('/q/:by', authApp, this.queryForEvents);
        app.get('/get/:by', authApp, this.getEventsList);
        app.get('/ping', this.testRoute);
        app.get('/location', authApp, this.getLocationList);
        app.get('/:id', authApp, this.getEventDetails);

        // app.post('/', authApp, this.addEvent);        
    }

    // ENDPOINTS //

    /**
     * @api {GET} /ping ping events api to see if it is online
     */
    async testRoute(req, res) {
        return await res.json({
            payload: crypto.randomBytes(10).toString('base64'),
            timestamp: new Date().toISOString()
        });
    }
    /**
     * @api {GET} /location query for a list of event locations
     */
    async getLocationList(req, res, next) {
        try {
            const eventLocationsList = await EventsModel.getLocation();
            return res.json(eventLocationsList);
        } catch(e) {
            return next(e);
        }
    }
    /**
     * @api {GET} /:id get event details
     */
    async getEventDetails(req, res, next) {
        try {
            const event = await EventsModel.getOne(req.params.id);
            return res.json(event);
        } catch(e) {
            return next(e);
        }
    }
    /**
     * @api {GET} /get/:by get a list of events by date or by location
     * @apiParam {string} loc
     * @apiParam {string} start_date
     * @apiParam {string} end_date
     */
    async getEventsList(req, res, next) {
        try {

            if (req.params.by === 'location' && req.query.event_loc) {
                const events = await EventsModel.get({ location: req.query.event_loc });
                return res.json(events);
            }

            if (req.params.by === 'date' && req.query.start_date) {
                const events = await EventsModel.getByDathe(req.query.start_date, req.query.end_date || null);
                return res.json(events);
            }

            // error out
            const err = new Error();
            err.name = 'QueryError';
            err.message = 'Could not get the list of events because of your request query. Please check /docs';
            err.status = 500;
            err.level = 'error';
            throw err;
        } catch(e) {
            return next(e);
        }
    }
    /**
     * @api {GET} /q/:by query for a paginated list of events by date or by location
     * @apiParam {string} loc
     * @apiParam {string} start_date
     * @apiParam {string} end_date
     * @apiParam {number} curr_page - The current page on the clients side
     */
    async queryForEvents(req, res, next) {
        try {

            if (req.params.by === 'location' && req.query.event_loc && req.query.curr_page) {
                const events = await EventsModel.query({ location: req.query.event_loc }, req.query.curr_page);
                return res.json(events);
            }

            if (req.params.by === 'date' && req.query.start_date && req.query.curr_page) {
                const events = await EventsModel.queryByDate({
                    sdt: req.query.start_date,
                    edt: req.query.end_date || null
                },
                req.query.curr_page);
                return res.json(events);
            }

            // error out
            const err = new Error();
            err.name = 'QueryError';
            err.message = 'Could not get the list of events because of your request query. Please check /docs';
            err.status = 500;
            err.level = 'error';
            throw err;
        } catch(e) {
            return next(e);
        }
    }
    /**
     * @api {POST} / record a new event
     */
    async addEvent(req, res, next) {
        try {
            if (req.body.event) {
                const event = await EventsModel.add(req.params.id);
                return res.json(event);
            }
            
            const err = new Error();
            err.name = 'RequestError';
            err.message = 'Request sent is missing data about the event';
            err.status = 400;
            err.level = 'error';
            throw err;
        } catch(e) {
            return next(e);
        }
    }

    //# ENDPOINTS //

}

module.exports = Api.init();