
require('dotenv').config();

const Osmosis = require('osmosis');
const Fs = require('fs');
const moment = require('moment');
const _ = require('lodash');

class EventsCrawler {

    /**
     * a crawler for capturing events on a given website
     * @param {Array} json 
     * @param {number} page 
     * @param {string} timestamp 
     * @param {Function} done 
     */
    crawl(json, page, timestamp, done) {
        Osmosis.post(
            'https://www.kenyabuzz.com/events/fetchEvents.php?calendar=' + timestamp,
            { page },
            {'Content-Type': 'application/x-www-form-urlencoded'}
        )
        .find('li .event-wrapper')
        .set({
            link: '.event-pic-wrapper a@href',
            image: '.event-pic-wrapper a img@src',
            date: '.event-description p[1]',
            time: '.event-description p[2]',
            title: '.event-description h4 > a'
        })
        .follow('.event-description h4 > a@href')
        .set({
            about: '.movie .movie__info .events-desc',
            map: '.movie .movie__info .map iframe@src'
        })
        .find('.movie .movie__info')
        .set({
            location: '.movie__option.events[3] a',
            location_link: '.movie__option.events[3] a@href'
        })
        .find('.movie .movie__info .share .in_article_social.descr')
        .set({
            twitter: 'a[2]@href',
            whatsapp: 'a[3]@href',
            google_plus: 'a[4]@href'
        })
        .data(dataCollected => json.push(this.serialize(dataCollected)))
        // .log(console.log)
        .error(e => done(e, undefined))
        .done(() => {
            done(null, json);
        });
    }

    /**
     * Runs the web crawler
     * @param {number} page The page which the crawler should crawl
     */
    run(page, timestamp='12/31/2016') {
        const events = [];
        return new Promise(
            (resolve, reject) => {
                this.crawl(events, page, timestamp,
                    (err, data) => {
                        if (err) reject(err);
                        resolve(data);
                    }
                );
            }
        );
    }

    /**
     * pauses the crawler to avoid stack overflow
     * @param {number} time how much time to pause
     * @param {string} returnTxt return message
     */
    pause(returnTxt, time=1000) {
        return new Promise(resolve => {
            _.delay(txt => {
                console.log(returnTxt);
                resolve()
            }, time, returnTxt);
        });
    }

    /**
     * serialize data collected by the web crawler into a \
     * format revelant to the database it will be store in
     * 
     * @param {*} e data collected by the web crawler
     */
    serialize(e) {
        let cache = {};
    
        const timeSplit = e.time.split(' - ');
        const startDate = `${e.date} ${timeSplit[0]}`
        const endDate = `${e.date} ${timeSplit[1]}`
        const Map = e.map.split('&')[0];
        
        cache = Object.assign({}, e, {
            start: moment(startDate, 'Do MMM, YYYY HH:mma').toISOString(),
            end: moment(endDate, 'Do MMM, YYYY HH:mma').toISOString(),
            map: Map
        });

        delete cache.date;
        delete cache.time;

        return cache;
    }

}

module.exports = EventsCrawler;