const mongoose = require('mongoose');
const { isEmail, isMobilePhone } = require('validator');

const appAuthorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        validate: {
            validator: v => v.split(' ').length >= 2,
            message: 'Requires at least two names'
        }
    },
    email: {
        type: String,
        unique: [true, 'Email address already exist among list of authors'],
        required: [true, 'Email is missing'],
        validate: {
            validator: v => isEmail(v),
            message: 'Email is invalid'
        } 
    },
    phone: {
        type: String,
        unique: [true, 'Phone number already exist among list of authors'],
        sparse: true,
        validate: {
            validator: v => isMobilePhone(v, 'any'),
            message: 'Phone number is invalid'
        } 
    }
});

const appsSchema = new mongoose.Schema({
    name:{ type: String, required: true, unique: true },
    platforms: { type: [String], required: true },
    appId: { type: String, required: true, unique: true },
    appSecret: { type: String, required: true, unique: true },
    authors: { type: [appAuthorSchema], required: true, unique: true },
    archived: { type: Boolean, default: false, required: true },
});

module.exports = mongoose.model('apps', appsSchema, 'Apps_Catalog');