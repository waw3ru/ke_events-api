require('dotenv').config();

const schema = require('./schema');
const mongoose = require('mongoose');
const crypto = require('crypto');
const _ = require('lodash');
const Hashids = require('hashids');

class Model {

    /**
     * initializes the class
     * @static
     */
    static init() {
        return new Model(schema);
    }

    /**
     * @constructor
     * @param {*} schema mongoose model
     */
    constructor(schema) {
        this._model=schema;
        this._hashids = new Hashids(process.env.TOKEN);
    }

    /**
     * tries to extract the appId from a str (App Secret)
     * @param {string} str string to extract object id
     */
    util(str) {
        try {
            const [appId, secret] = str.split('||');
            const id = this._hashids.decodeHex(appId);
            return !_.isNil(id) || !_.isEmpty(id) ? id: 'no such id was hashed';

        } catch(e) {
            const err = new Error();
            err.name =  e.name || 'DecodeError';
            err.message = e.message || 'Failed to perform a confimation security check.';
            err.status = 500;
            err.level = 'error';
            return err;
        }
    }
    /**
     * record a new app to db
     * @param {string} name 
     * @param {Array} platforms 
     * @param {Array} authors 
     */
    add(name, platforms=[], authors=[]) {
        const app = new this._model({ name });
        app.authors = [].concat(...authors);
        app.platforms = [].concat(...platforms);
        app.appId = crypto.randomBytes(10).toString('hex');
        app.appSecret = this._hashids.encodeHex(app.appId) + '||' + this._hashids.encodeHex(crypto.randomBytes(20).toString('hex'));

        return app.save()
        .catch(e => {
            const err = new Error();
            err.status = 500;
            err.level = 'error';

            if (e && e.code===11000) {
                err.name = 'DuplicateRecord';
                err.message = 'The app seems to already exist. Please review your data.';
                return Promise.reject(err);
            }

            err.name = 'CreationError';
            err.message = e.message || 'Something went wrong while saving events';
            return Promise.reject(err);
        });
    }
    /**
     * returns a list of apps based on a query
     * @param {Object} q 
     */
    get(q=null) {
        return this._model.find(q || {}).exec();
    }
    /**
     * returns a single app record
     * @param {string} id 
     */
    getOne(id) {
        return this._model.findOne({ _id: id }).exec();
    }
    /**
     * updates an app's record
     * @param {string} id 
     * @param {Object} updates 
     */
    update(id, appId, updates) {
        const changes = Object.assign({}, updates);

        if (_.has(changes, 'appId')) {
            delete changes.appId;
        }
        if (_.has(changes, 'appSecret')) {
            delete changes.appSecret;
        }

        return this._model.findOne({ appId, _id: id }).exec()
            .then(doc => {
                const { name, author, platform, archived } = changes;

                if (archived) doc.archived = archived;
                if (name) doc.name = name;

                if (!_.isNil(authors) && !_.isEmpty(authors)) {
                    doc.authors.push(author);
                }
                if (!_.isNil(platforms) && !_.isEmpty(platforms)) {
                    doc.platforms.push(platform);
                }

                return doc.save();
            });
    }
    /**
     * delete app from db
     * @param {string} appId
     * @param {string} appSecret
     */
    remove(appId, appSecret) {
        return this._model.findOneAndRemove({ appId, appSecret }).exec();
    }
    /**
     * 
     * @param {*} appId 
     */
    updateAppSecret(appId) {
        return this._model.findOneAndUpdate({ appId },
            { appSecret: this._hashids.encodeHex(appId + '||' + crypto.randomBytes(20).toString('hex')) },
            { new: true, runValidators: true })
        .exec()
        .then(doc => Promise.resolve({ newSecret: doc.appSecret }));
    }
}

module.exports = Model.init();