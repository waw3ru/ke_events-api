const _ = require('lodash');
const express = require('express');
const AppsModel = require('./model');
const { authApp } = require('./util');
const Agenda = require('../../worker/agenda');

class Api {

    /**
     * @static
     * initializes the class
     */
    static init() {
        const router = express.Router();
        new Api(router);
        return router;
    }

    /**
     * @constructor
     * @param {*} app express router class
     */
    constructor(app) {
        app.delete('/', authApp, this.deleteApp);

        app.get('/ping', this.testRoute);
        app.get('/q', this.queryApps);
        app.get('/:id', this.getApp);

        app.post('/', this.createApp);

        app.put('/edit/secret', authApp, this.updateAppSecret);
        app.put('/edit/:id', authApp, this.updateApp);
    }

    /**
     * @api {GET} /ping - Ping apps api to see if it is online
     */
    async testRoute(req, res) {
        return await res.json({
            payload: crypto.randomBytes(12).toString('base64'),
            timestamp: new Date().toISOString()
        });
    }
    /**
     * @api {POST} /create - Creates a new App
     */
    async createApp(req, res, next) {
        try {
            if (req.body && req.body.app_details && req.body.secret === 'john.8242') {
                const { name, authors, platforms } = req.body.app_details
                const app = await AppsModel.add(name, platforms || [], authors || [])
                    .then(app => {
                        app.authors.forEach(author => {
                            Agenda.now('send e-mail', {
                                to: { name: author.name, email: author.email },
                                from: { name: 'Wakenya Wawili', email: 'waweruj00@gmail.com' },
                                subject: '[no-reply] Your API Key',
                                templateId: '0c7fdff8-ab23-47d0-91ac-bd21b8d25877',
                                data: {
                                    author: `${author.name}`,
                                    app_id: `${app.appId}`,
                                    app_secret: `${app.appSecret}`
                                }
                            });
                        });
                        // app data
                        return app;
                    });

                return res.json(app);
            }

            const err = new Error();
            err.name = 'RequestError';
            err.message = 'Request sent is missing data about the app';
            err.status = 400;
            err.level = 'error';
            throw err;
        } catch (e) {
            return next(e);
        }
    }
    /**
     * @api {GET} /q - Query's for a list of apps
     */
    async queryApps(req, res, next) {
        try {
            const apps = await AppsModel.get((!_.isNil(req.query)) ? req.query : null);
            return res.json(apps);
        } catch (e) {
            return next(e);
        }
    }
    /**
     * @api {GET} /:id - Gets an app profile
     */
    async getApp(req, res, next) {
        try {
            const app = await AppsModel.get(req.params.id);
            return res.json(app);
        } catch (e) {
            return next(e);
        }
    }
    /**
     * @api {PUT} /edit/:id - updates an app's profile
     */
    async updateApp(req, res, next) {
        try {
            const app = await AppsModel.update(req.params.id, req.events254_app.appId, req.body.update);
            return res.json(app);
        } catch (e) {
            return next(e);
        }
    }
    /**
     * @api {PUT} /edit/secret - updates app secret
     */
    async updateAppSecret(req, res, next) {
        try {
            const app = await AppsModel.updateAppSecret(req.events254_app.appId);
            return res.json(app);
        } catch (e) {
            return next(e);
        }
    }
    /**
     * @api {DELETE} /:id - deletes an app
     */
    async deleteApp(req, res, next) {
        try {
            if (req.body && req.body.app_secret) {
                const app = await AppsModel.remove(req.events254_app.appId, req.body.app_secret);
                return res.json(app);
            }

            const err = new Error();
            err.name = 'RequestError';
            err.message = 'Request sent is missing data about the app';
            err.status = 400;
            err.level = 'error';
            throw err;
        } catch (e) {
            return next(e);
        }
    }

}

module.exports.api = Api.init();