
const AppsModel = require('./model');

/**
* Authorizes every http request to see if the app has been given access
* @param {Object} req express request object
* @param {Object} res express response object
* @param {Function} next express next function
*/
module.exports.authApp = async (req, res, next) => {
   try {
       if (req.headers && !req.headers['x-auth-app']) {
           const err = new Error();
           err.name = 'AuthorizationError';
           err.message = 'App credentials are missing please visit documentation.';
           err.status = 401;
           err.level = 'critical';
           throw err;
       }

       const app_id = req.headers['x-auth-app'].trim();
       const apps = await AppsModel.get({
           appId: AppsModel.util(req.headers['x-auth-app']),
           appSecret: req.headers['x-auth-app']
        });

       if (!Array.isArray(apps) || apps.length!==1) {
           const err = new Error();
           err.name = 'AuthorizationError';
           err.message = 'App could not be authenticated due to wrong credentials.';
           err.status = 401;
           err.level = 'critical';
           throw err;
       }

       // register app details on request object
       req.events254_app = Object.assign({}, apps[0]);
       return next();
   } catch (e) {
       return next(e);
   }
};